<?php

namespace Betkey\DatetimeBundle\Service;

use DateTime;
use DateTimeZone;
use MongoDB\BSON\UTCDateTime;

class TimezoneManager
{
    const UTC_TIMEZONE = 'UTC';

    /**
     * @var int
     */
    private $min;

    /**
     * @var int
     */
    private $max;

    /**
     * @var bool
     */
    private $positive;

    /**
     * TimezoneManager constructor.
     *
     * @param int $min
     * @param int $max
     * @param bool $positive
     */
    public function __construct(int $min, int $max, bool $positive)
    {
        $this->min = $min;
        $this->max = $max;
        $this->positive = $positive;
    }

    /**
     * Get random integer.
     *
     * @return int
     *
     * @throws \Exception
     */
    public function getRandom(): int
    {
        $number = random_int($this->min, $this->max);

        if (!$this->positive) {
            $number = -$number;
        }

        return $number;
    }

    /**
     * Convert datetime string to UTCDateTime object.
     *
     * @param string $datetime
     * @param string $instanceTimezone
     *
     * @return UTCDateTime
     */
    public static function toUTCDatetime(string $datetime, string $instanceTimezone): UTCDateTime
    {
        $datetime = static::changeDatetimeZone(
            $datetime,
            $instanceTimezone,
            static::UTC_TIMEZONE
        );

        return static::timestampToUtcDatetime($datetime->getTimestamp());
    }

    /**
     * Change the TimeZone on DateTime object.
     *
     * @param string $datetime
     * @param string $fromTimezone
     * @param string $toTimezone
     *
     * @return DateTime
     */
    public static function changeDatetimeZone(string $datetime, string $fromTimezone, string $toTimezone): DateTime
    {
        $datetime = new DateTime($datetime, new DateTimeZone($fromTimezone));

        return $datetime->setTimezone(new DateTimeZone($toTimezone));
    }

    /**
     * Convert timestamp to UTCDateTime object.
     *
     * @param int $timestamp
     *
     * @return UTCDateTime
     */
    public static function timestampToUtcDatetime(int $timestamp): UTCDateTime
    {
        return new UTCDateTime($timestamp * 1000);
    }
}
