<?php

namespace Betkey\DatetimeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Betkey\DatetimeBundle\DependencyInjection\BetkeyDatetimeExtension;

class BetkeyDatetimeBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new BetkeyDatetimeExtension();
        }

        return $this->extension;
    }

}
