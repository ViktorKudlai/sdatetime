<?php

namespace Betkey\DatetimeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @inheritdoc
     *
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('betkey_datetime');

        $rootNode
            ->children()
                ->integerNode('min')->defaultValue(1000)->info('Starting point.')->end()
                ->integerNode('max')->defaultValue(1000000)->info('Ending point.')->end()
                ->booleanNode('positive')->defaultTrue()->info('Positive number or negative.')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
